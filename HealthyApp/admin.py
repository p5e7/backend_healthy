from django.contrib import admin

# Register your models here.
from HealthyApp.models.user import User
from HealthyApp.models.enfermero import Enfermero
from HealthyApp.models.servicios import Servicios
from HealthyApp.models.tiposdeservicios import TiposDeServicios

admin.site.register(User) # SE REGISTRAN LOS MODULOS EN LA APLICACION
admin.site.register(Enfermero)
admin.site.register(Servicios)
admin.site.register(TiposDeServicios)
