from rest_framework import serializers
from HealthyApp.models.servicios import Servicios

class ServiciosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Servicios
        fields = ['id','description','enfermero','servicio']