from rest_framework import serializers
from HealthyApp.models.contrato import Contrato
from HealthyApp.models.transaction import Transaction
from HealthyApp.models.enfermero import Enfermero
from HealthyApp.models.user import User
class ContratoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contrato
        fields = ['id','description','enfermero','user'] #id del contrato , descripcion, enfermero que es contratado, usuario que contrata

    def to_representation(self, obj):
        contrato = Contrato.objects.get(id=obj.id)
        transaction = Transaction.objects.get(id=obj.transaction_id)
        enfermero = Enfermero.objects.get(id=obj.enfermero_id)
        user = User.objects.get(id=obj.user_id)
        return {
            'id': contrato.id,
            'description': contrato.description,
            'enfermero': enfermero.nombre,
            'user': user.name,
            'transaction': {
                'note': transaction.note,
                'metodo_pago':transaction.metodo_pago,
                'register_date': transaction.register_date
            }
        }