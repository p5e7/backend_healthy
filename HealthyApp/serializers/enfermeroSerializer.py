from rest_framework import serializers
import rest_framework
from HealthyApp.models.enfermero import Enfermero

from HealthyApp.models.servicios import Servicios
from HealthyApp.models.tiposdeservicios import TiposDeServicios

from HealthyApp.models.calificacion import Calificacion

from django.db.models import Avg

class EnfermeroSerializer(serializers.ModelSerializer):
    class Meta:
        #QUE MODELO QUEREMOS SERIALIZAR ? ->
        model = Enfermero # PONEMOS EL MODELO QUE QUEREMOS SERIALIZAR
        fields = ['id','nombre','email','contacto','especialidad','calificacion','description'] # CAMPOS QUE TIENE EL MODELO, QUE QUEREMOS SERIALIZAR

    def to_representation(self, obj):
        
        enfermero = Enfermero.objects.get(id=obj.id)
        servicios = Servicios.objects.filter(enfermero_id=obj.id)
        calificacion = Calificacion.objects.filter(enfermero_id=obj.id).aggregate(Avg('calificacion'))
        if(calificacion['calificacion__avg'] is None  ):
            enfermero.calificacion = 5
        else:
            enfermero.calificacion = calificacion['calificacion__avg']

        dic = {}
        costoServicio = 0
        x = 0
        for i in servicios:
            x+=1
            tiposervicios = TiposDeServicios.objects.get(id=i.servicio_id)
            costoServicio += tiposervicios.costo_hora
            dic['Servicio '+ str(x)] = tiposervicios.tipo_de_servicio
            #dic['Descripcion '+ str(x)]  = i.description
        ruta_ab = f'/{enfermero.nombre}/{enfermero.id}/'
        if(x!=0):
            costoServicio= costoServicio/x
        else: costoServicio = 50000
        return {
            "id":enfermero.id,
            "nombre":enfermero.nombre,
            "especialidad":enfermero.especialidad,
            "contacto":enfermero.contacto,
            "email":enfermero.email,
            "calificacion":enfermero.calificacion,
            "descripcion":enfermero.description,
            "tipos_de_servicio": dic,
            "ruta_absoluta": ruta_ab,
            "costo_servicio":costoServicio
        }