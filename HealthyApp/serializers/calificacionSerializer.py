from django.db.models.base import Model
from rest_framework import serializers
from HealthyApp.models.calificacion import Calificacion

class CalificacionSerializer(serializers.ModelSerializer):
    class Meta:
        model=Calificacion
        fields = ['id','description','calificacion','enfermero','user'] # ID DEL CONTRATO, DESCRIPCION, ENFERMERO QUE SE CONTRATÓ, USUARIO QUE LO CONTRATA.  , este modelo se creo para guardad una calificacion al enfermero