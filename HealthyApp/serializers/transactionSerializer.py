from rest_framework import serializers
from HealthyApp.models import transaction
from HealthyApp.models.transaction import Transaction
from HealthyApp.models.contrato import Contrato
from HealthyApp.serializers import ContratoSerializer
class TransactionSerializer(serializers.ModelSerializer):
    contrato = ContratoSerializer()
    class Meta:
        model = Transaction
        fields = ['id','register_date','note','metodo_pago','contrato']  #id de la transaccion, contrato que es foranea al contrato el cual se realizo el pago, registro de la fecha, una nota , y el metodo de pago efectuado
    
    def create(self, validated_data):  # CREA LOS MODELOS ACCOUNT Y USER en la BD
        contratoData = validated_data.pop('contrato')
        transactionInstance = Transaction.objects.create(**validated_data)
        Contrato.objects.create(transaction=transactionInstance, **contratoData)
        return transactionInstance

    def to_representation(self, obj):
        transactions = Transaction.objects.get(id=obj.id)
        
        return {
            'id': transactions.id,
            'register_date': transactions.register_date,
            'note': transactions.note,
            'metodo_pago': transactions.metodo_pago,
        }