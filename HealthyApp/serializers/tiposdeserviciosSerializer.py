from rest_framework import serializers
from HealthyApp.models.tiposdeservicios import TiposDeServicios

class TiposDeServiciosSerializer(serializers.ModelSerializer):
    class Meta:
        model = TiposDeServicios
        fields = ['id','tipo_de_servicio','description','costo_hora']