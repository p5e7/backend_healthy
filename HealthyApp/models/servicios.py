from django.db import models
from .tiposdeservicios import TiposDeServicios
from .enfermero import Enfermero
# INDENTIDAD DEBIL
class Servicios(models.Model):
    id = models.AutoField(primary_key=True) #ID UNICO DEL registro del servicio que tiene el enfermero
    description = models.TextField('description',max_length=200,blank=True) #DESCRIPCION DEL CONTRATO
    enfermero = models.ForeignKey(Enfermero,related_name='enfermero_servicio',on_delete=models.CASCADE) #LLAVE FORANEA QUE APUNTA A UN ENFERMERO
    servicio = models.ForeignKey(TiposDeServicios,related_name= 'tipo_servicio',on_delete=models.CASCADE) #LLAVE FORANEA QUE APUNTA AL TIPO DE SERVICIO