from django.db import models
from .user import User
from .enfermero import Enfermero

class Calificacion(models.Model):
    id = models.AutoField(primary_key=True) #ID UNICO DEL CONTRATO
    description = models.TextField('description',max_length=200,blank=True) #DESCRIPCION DEL CONTRATO
    enfermero = models.ForeignKey(Enfermero,related_name='enfermero_calificado',on_delete=models.CASCADE) #LLAVE FORANEA QUE APUNTA A UN ENFERMERO
    user = models.ForeignKey(User,related_name= 'user_calificador',on_delete=models.CASCADE) #LLAVE FORANEA QUE APUNTA A UN USUARIO
    calificacion = models.IntegerField('calificacion',null=False) #CALIFICACION DEL ENFERMERO

