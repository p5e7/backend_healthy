from django.db import models

from django.utils import timezone


class Transaction(models.Model):
    id = models.AutoField(primary_key=True)
    
    register_date = models.DateTimeField(default=timezone.now, blank=True)

    note = models.CharField(max_length=100,blank=True)

    metodo_pago = models.CharField(max_length=50,null=False)

    
