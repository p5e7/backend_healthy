from django.db import models

from HealthyApp.models.user import UserManager
class Enfermero(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField('name', max_length=40 ) # NOMBRE DEL ENFERMERO 
    email = models.EmailField('Email', max_length = 100) # email del usuario
    contacto = models.CharField('contacto', max_length=20) # CONTACTO DEL USUSARIO -> CELULAR
    especialidad = models.CharField('especialidad',max_length = 40) # ESPECIALIDAD QUE TIENE EL ENFERMERO
    calificacion = models.IntegerField('calificacion',default=0) #CALIFICACION DEL ENFERMERO
    description = models.TextField('description',max_length=200)
