from django.db import models

from HealthyApp.models.transaction import Transaction
from .user import User
from .enfermero import Enfermero
from .transaction import Transaction

class Contrato(models.Model):
    id = models.AutoField(primary_key=True) #ID UNICO DEL CONTRATO
    description = models.TextField('description',max_length=200) #DESCRIPCION DEL CONTRATO
    enfermero = models.ForeignKey(Enfermero,related_name='contrato_enfermero',on_delete=models.CASCADE) #LLAVE FORANEA QUE APUNTA A UN ENFERMERO
    user = models.ForeignKey(User,related_name= 'contrato_usuario',on_delete=models.CASCADE) #LLAVE FORANEA QUE APUNTA A UN USUARIO
    
    transaction = models.ForeignKey(Transaction, related_name='Transaction',on_delete=models.CASCADE)