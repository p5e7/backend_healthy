from django.db import models

class TiposDeServicios(models.Model):
    id = models.AutoField(primary_key=True) #ID UNICO DEL CONTRATO
    tipo_de_servicio = models.TextField('description',max_length=200, null=False) #TIPO DE SERVICIO
    description = models.TextField('description',max_length=200,blank=False,null = False) #DESCRIPCION DEL CONTRATO
    costo_hora = models.DecimalField('costo_hora', max_digits = 20, decimal_places=3)

