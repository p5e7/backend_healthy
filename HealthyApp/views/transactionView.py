from django.conf import settings
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.backends import TokenBackend

from HealthyApp.models.transaction import Transaction
from HealthyApp.serializers.transactionSerializer import TransactionSerializer



class TransactionCreateView(generics.CreateAPIView):
    serializer_class = TransactionSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        print("Request:", self.request)
        print("Args:", self.args)
        print("KWArgs:", self.kwargs)

        token = self.request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token, verify=False)
        print(valid_data)
        if valid_data['user_id'] != request.data['user_id']:
            stringResponse = {'detail': 'Unauthorized request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)

        serializer = TransactionSerializer(
            data=request.data['transaction_data']) # ACA SE ENVIA LA DATA PARA CREAR LA TRANSACTION CORRECTAMENTE
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response('Transacción esxitosa, se ha realizado el contrato correctamente', status=status.HTTP_201_CREATED)