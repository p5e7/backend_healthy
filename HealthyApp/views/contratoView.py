from django.conf import settings
from rest_framework import generics,status # LIBRERIA para hacer el crud 
from rest_framework.response import Response # hacer un response de alguna data , mensaje, etc..
from rest_framework.permissions import IsAdminUser, IsAuthenticated # para comprobar si un modelo está autenticado y poder hacer su respectivo uso 
from rest_framework_simplejwt.backends import TokenBackend # para validacion de token

#modelo y serializador 
from HealthyApp.models.contrato import Contrato  # MODELO DE ENFERMERO 
from HealthyApp.serializers.contratoSerializer import ContratoSerializer # SERIALIZADOR

# INFORMACION DE LA TRANSACCION
class ContratoCreatedView(generics.CreateAPIView):
    serializer_class = ContratoSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        print("Request:", self.request)
        print("Args:", self.args)
        print("KWArgs:", self.kwargs)

        token = self.request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token, verify=False)

        if valid_data['user_id'] != request.data['user_id']:
            stringResponse = {'detail': 'Unauthorized request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)

        serializer = ContratoSerializer(
            data=request.data['contrato_data']) # ACA DEBE VENIR LA DATA DEL PAGO TAMBIEN PARA PODER GENERAR EL CONTRATO.
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response('Contrato hecho exitosamente!', status=status.HTTP_201_CREATED)



# ver los contratos
class ContratosAccountView(generics.ListAPIView):
    serializer_class = ContratoSerializer
    permission_classes = (IsAuthenticated,)

    # codigo para hacer una verificacion de toquen , hacer una busqueda filtrada y retornarla
    def get_queryset(self):
        print("Request:", self.request)
        print("Args:", self.args)
        print("KWArgs:", self.kwargs)

        token = self.request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token, verify=False)

        if valid_data['user_id'] != self.kwargs['user']:
            stringResponse = {'detail': 'Unauthorized request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        # origin_account_id es el nombre de la llave foranea que le puso django al hacer la migracion , por eso usamos este campo
        queryset =  Contrato.objects.filter(
            user_id=self.kwargs['user'])
        return queryset