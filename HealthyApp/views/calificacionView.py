from django.conf import settings #IMPORTO LOS SETTINGS DEL PROYECTO
from rest_framework import generics,status # LIBRERIA para hacer el crud 
from rest_framework.response import Response # hacer un response de alguna data , mensaje, etc..
from rest_framework.permissions import IsAdminUser, IsAuthenticated # para comprobar si un modelo está autenticado y poder hacer su respectivo uso 
from rest_framework_simplejwt.backends import TokenBackend # para validacion de token

#modelo y serializador 
from HealthyApp.models.calificacion import Calificacion  # MODELO DE CALIFICACION
from HealthyApp.serializers.calificacionSerializer import CalificacionSerializer

from HealthyApp.models.contrato import Contrato
from HealthyApp.serializers.contratoSerializer import ContratoSerializer

class   CalificacionCreateView(generics.CreateAPIView):
    serializer_class = CalificacionSerializer
    serializer_class2 = ContratoSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        print("Request:", self.request)
        print("Args:", self.args)
        print("KWArgs:", self.kwargs)

        token = self.request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token, verify=False)
        print(valid_data)
        queryset = Contrato.objects.filter(user_id = valid_data['user_id']) ##TRAIGO TODOS LOS CONTRATOS QUE TIENE ESTE USUARIO, SI TIENE ALMENOS UNO, ES VALIDO
        
        if valid_data['user_id'] != request.data['user']:
            stringResponse = {'detail': 'Unauthorized request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)


        if(len(queryset)>0):
            serializer = CalificacionSerializer(data=request.data) # ACA SE ENVIA LA DATA PARA CREAR LA TRANSACTION CORRECTAMENTE
            serializer.is_valid(raise_exception=True)
            serializer.save()


            return Response('Calificacion exitosa, se ha realizado el contrato correctamente', status=status.HTTP_201_CREATED)
        else:
            stringResponse = {'detail': 'Unauthorized request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
            