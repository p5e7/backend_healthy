from django.conf import settings
from django.db.models.query import QuerySet # configuracion del proyecto 
from rest_framework import generics,status # LIBRERIA para hacer el crud 
from rest_framework.response import Response # hacer un response de alguna data , mensaje, etc..
from rest_framework.permissions import IsAdminUser, IsAuthenticated # para comprobar si un modelo está autenticado y poder hacer su respectivo uso 
from rest_framework_simplejwt.backends import TokenBackend # para validacion de token

#modelo y serializador 
from HealthyApp.models.enfermero import Enfermero  # MODELO DE ENFERMERO 
from HealthyApp.serializers.enfermeroSerializer import EnfermeroSerializer # SERIALIZADOR


#QUE FUNCIONALIDADES VAMOS A TENER CON ESTE MODELO?
# 1-> MOSTRAR INFORMACION
# 2-> CREAR ENFERMERO 
# 3 -> ELIMINAR ENFERMERO 
# 4 -> ACTUALIZAR DATA DEL ENFERMERO 

#FUNCION CREAR UN ENFERMERO
class EnfermeroCreate(generics.CreateAPIView):
    serializer_class = EnfermeroSerializer
    permission_classes = (IsAdminUser,)

    def post(self, request, *args, **kwargs):
        print("Request:", self.request)
        print("Args:", self.args)
        print("KWArgs:", self.kwargs)

        serializer = EnfermeroSerializer(
            data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response('Enfermero created', status=status.HTTP_201_CREATED)






#DEVUELVE LA INFORMACION DE UN ENFERMERO en el url se pide por id de enfermero
"""class EnfermeroDetailView(generics.ListAPIView): # retrieve es para traer la informacion
    serializer_class = EnfermeroSerializer
    def get_queryset(self):     
        queryset = Enfermero.objects.filter(id = self.kwargs['pk'])
        return queryset"""


class EnfermeroDetailView(generics.RetrieveAPIView):
    queryset = Enfermero.objects.all()
    serializer_class =EnfermeroSerializer

    def get(self, request, *args, **kwargs):

        return super().get(request, *args, **kwargs)
        

#CLASE QUE DA LA FUNCION QUE ELIMINA A UN ENFERMERO DE LA BASE DE DATOS
class EnfermeroDelete(generics.DestroyAPIView):  
    serializer_class = EnfermeroSerializer # serializador del enfermero
    permission_classes = (IsAdminUser,) #permiso solo para un administrador, pongo el isauthenticates solo para facilidad en las pruebas.
    queryset = Enfermero.objects.all() # LLAMO TODOS LOS ENFERMEROS QUE ESTAN EN LA BASE DE DATOS

    def get(self, request, *args, **kwargs):
        print("Request:", self.request)
        print("Args:", self.args)
        print("KWArgs:", self.kwargs)
        return super().destroy(request, *args, **kwargs)


#clASE QUE DA LA FUNCION DE ACTUALIZAR DATO DE UN ENFERMERO
class EnfermeroUpdate(generics.UpdateAPIView): 
    serializer_class = EnfermeroSerializer
    permission_classes = (IsAdminUser,)
    queryset = Enfermero.objects.all()
    def put(self, request, *args, **kwargs):
        
        return super().partial_update(request, *args, **kwargs) #partial_update , actualiza uno o varios elementos del enfermero, no necesariamente tiene que ser todo 

#FUNCION OBJETER TODOS LOS ENFERMEROS 
class EnfermerosView(generics.ListAPIView):
    serializer_class = EnfermeroSerializer
    def get_queryset(self):
        queryset = Enfermero.objects.all()
        return queryset
