from .userCreateView import UserCreateView
from .userDetailView import UserDetailView
from .enfermeroView import EnfermeroDetailView
from .contratoView import  ContratoCreatedView
from .transactionView import TransactionCreateView
from .calificacionView import CalificacionCreateView