from django.contrib import admin
from django.urls import path
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from HealthyApp import views as HealthyAppViews
from HealthyApp.views import enfermeroView
from HealthyApp.views import contratoView
from HealthyApp.views import transactionView
from HealthyApp.views import calificacionView

urlpatterns = [
    path('admin/', admin.site.urls), # PATH PARA CREAR EL ADMINISTRADOR



        # funcionalidad que da el simple-jwt
    path('login/', TokenObtainPairView.as_view()),
    # funcionalidad del simplejwt
    path('refresh/', TokenRefreshView.as_view()),



    # funcionalidad agregada de crear el usuario
    path('user/', HealthyAppViews.UserCreateView.as_view()),
    # funcionalidad de mostrar la informacion de un usuario
    path('user/<int:pk>/', HealthyAppViews.UserDetailView.as_view()),



    # FUNCIONALIDAD , TRAER TODA LA INFORMACION DE UN ENFERMERO
    path('enfermero/<int:pk>/', HealthyAppViews.enfermeroView.EnfermeroDetailView.as_view()), # GET
    # FUNCIONALIDAD, ELIMINAR UN ENFERMERO DE LA BASE DE DATOS
    path('enfermero/remove/<int:pk>/', HealthyAppViews.enfermeroView.EnfermeroDelete.as_view() ),#GET
    #FUNCIONALIDAD, CREAR UN ENFERMERO, SOLO LOS ADMINISTRADORES PUEDEN HACERLO
    path('enfermero/',HealthyAppViews.enfermeroView.EnfermeroCreate.as_view()), #METODO POST
    #funcionalidad, actualizar la informacion de un enfermero
    path('enfermero/update/<int:pk>',HealthyAppViews.enfermeroView.EnfermeroUpdate.as_view()), #METODO POST
    #funcionalidad, mostrar la informacion de todos los enfermeros
    path('enfermeros/' , HealthyAppViews.enfermeroView.EnfermerosView.as_view()), # Metodo get, queryset

    #funcionalidad, ver todos los contratos
    path('contratos/<int:user>', HealthyAppViews.contratoView.ContratosAccountView.as_view()),

    # funcionalidad crear una transaccion y su respectivo contrato
    path('transaction/', HealthyAppViews.transactionView.TransactionCreateView.as_view()),  # create a new transaction



    #FUNCIONALIDAD CALIFICACION
    path('calificar/', HealthyAppViews.calificacionView.CalificacionCreateView.as_view()), #GENERA UNA CALIFICACION A UN ENFERMERO
]
